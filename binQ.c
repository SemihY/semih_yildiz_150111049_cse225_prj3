#include "binomial.h"
#include "fatal.h"


/* START: fig6_52.txt */
typedef struct BinNode *Position;

struct BinNode
{
	ElementType Item;
	ElementType Time;
	bool ekle;
	//model_node_type mn;
	Position    LeftChild;
	Position    NextSibling;
};

struct Collection
{
	double CurrentSize;
	BinTree TheTrees[ MaxTrees ];
};

BinQueue
Initialize( void )
{
	BinQueue H;
	int i;

	H = malloc( sizeof( struct Collection ) );
	if( H == NULL )
		FatalError( "Out of space!!!" );
	H->CurrentSize = 0;
	for( i = 0; i < MaxTrees; i++ )
		H->TheTrees[ i ] = NULL;
	return H;
}

static void
DestroyTree( BinTree T )
{
	if( T != NULL )
	{
		DestroyTree( T->LeftChild );
		DestroyTree( T->NextSibling );
		free( T );
	}
}

void
Destroy( BinQueue H )
{
	int i;

	for( i = 0; i < MaxTrees; i++ )
		DestroyTree( H->TheTrees[ i ] );
}

BinQueue
MakeEmpty( BinQueue H )
{
	int i;

	Destroy( H );
	for( i = 0; i < MaxTrees; i++ )
		H->TheTrees[ i ] = NULL;
	H->CurrentSize = 0;

	return H;
}

/* Not optimized for O(1) amortized performance */
BinQueue
Insert( ElementType Item, ElementType Time,bool sa, BinQueue H )
{
	BinTree NewNode;
	BinQueue OneItem;
	int i;

	NewNode = malloc( sizeof( struct BinNode ) );
	if( NewNode == NULL )
		FatalError( "Out of space!!!" );
	NewNode->LeftChild = NewNode->NextSibling = NULL;
	NewNode->Item = Item;
	NewNode->Time = Time;
	/*NewNode->mn.p = m.p;
	NewNode->mn.sid.scalar = m.sid.scalar;
	NewNode->mn.aid.d3act = m.aid.d3act;
	NewNode->mn.nexts.scalar = m.nexts.scalar;
	NewNode->mn.reward = m.reward;
	NewNode->mn.last=m.last;
	for (i=0; i < max_act_const; i++) {
	  NewNode->mn.from[i].s=m.from[i].s;
	  NewNode->mn.from[i].a=m.from[i].a;
	}*/  

	OneItem = Initialize( );
	OneItem->CurrentSize = 1;
	OneItem->TheTrees[ 0 ] = NewNode;

	return Merge( H, OneItem );
}

/* START: fig6_56.txt */
ElementType 
DeleteMin( BinQueue H )
{
	int i, j;
	int MinTree;   /* The tree with the minimum item */
	BinQueue DeletedQueue;
	Position DeletedTree, OldRoot;
	ElementType MinItem;
	//model_node_type MinItem;

	if( IsEmpty( H ) )
	{
		Error( "Empty binomial queue" );
		MinItem=-Infinity;
		
	//  for (i=0; i < max_act_const; i++) {
	//    MinItem.from[i].s=-1;
	//    MinItem->mn.from[i].a=m.from[i].a;
	//  }
		return MinItem;
	}

	MinItem = Infinity;
	for( i = 0; i < MaxTrees; i++ )
	{
		if( H->TheTrees[ i ] &&
			H->TheTrees[ i ]->Item < MinItem )
		{
			/* Update minimum */
			MinItem = H->TheTrees[ i ]->Item;
			
			MinTree = i;
		}
	}

	DeletedTree = H->TheTrees[ MinTree ];
	OldRoot = DeletedTree;
	DeletedTree = DeletedTree->LeftChild;
	free( OldRoot );

	DeletedQueue = Initialize( );
	DeletedQueue->CurrentSize = ( 1 << MinTree ) - 1;
	for( j = MinTree - 1; j >= 0; j-- )
	{
		DeletedQueue->TheTrees[ j ] = DeletedTree;
		DeletedTree = DeletedTree->NextSibling;
		DeletedQueue->TheTrees[ j ]->NextSibling = NULL;
	}

	H->TheTrees[ MinTree ] = NULL;
	H->CurrentSize -= DeletedQueue->CurrentSize + 1;

	Merge( H, DeletedQueue );
	return MinItem;
}
/* END */

ElementType
FindMin( BinQueue H )
{
	int i;
	ElementType MinItem;

	if( IsEmpty( H ) )
	{
		Error( "Empty binomial queue" );
		MinItem=0;
		return MinItem;
	}

	MinItem = Infinity;
	
	for( i = 0; i < MaxTrees; i++ )
	{
		if( H->TheTrees[ i ] &&
					H->TheTrees[ i ]->Item < MinItem ) {
			MinItem = H->TheTrees[ i ]->Item;
		}	
	}

	return MinItem;
}

int
IsEmpty( BinQueue H )
{
	return H->CurrentSize == 0;
}

int IsFull( BinQueue H )
{
	return H->CurrentSize == Capacity;
}

/* START: fig6_54.txt */
/* Return the result of merging equal-sized T1 and T2 */
BinTree
CombineTrees( BinTree T1, BinTree T2 )
{
	if( T1->Item > T2->Item )
		return CombineTrees( T2, T1 );
	T2->NextSibling = T1->LeftChild;
	T1->LeftChild = T2;
	return T1;
}
/* END */

/* START: fig6_55.txt */
/* Merge two binomial queues */
/* Not optimized for early termination */
/* H1 contains merged result */

BinQueue
Merge( BinQueue H1, BinQueue H2 )
{
	BinTree T1, T2, Carry = NULL;
	int i, j;

	if( H1->CurrentSize + H2->CurrentSize > Capacity )
		Error( "Merge would exceed capacity" );

	H1->CurrentSize += H2->CurrentSize;
	for( i = 0, j = 1; j <= H1->CurrentSize; i++, j *= 2 )
	{
		T1 = H1->TheTrees[ i ]; T2 = H2->TheTrees[ i ];

		switch( !!T1 + 2 * !!T2 + 4 * !!Carry )
		{
			case 0: /* No trees */
			case 1: /* Only H1 */
				break;
			case 2: /* Only H2 */
				H1->TheTrees[ i ] = T2;
				H2->TheTrees[ i ] = NULL;
				break;
			case 4: /* Only Carry */
				H1->TheTrees[ i ] = Carry;
				Carry = NULL;
				break;
			case 3: /* H1 and H2 */
				Carry = CombineTrees( T1, T2 );
				H1->TheTrees[ i ] = H2->TheTrees[ i ] = NULL;
				break;
			case 5: /* H1 and Carry */
				Carry = CombineTrees( T1, Carry );
				H1->TheTrees[ i ] = NULL;
				break;
			case 6: /* H2 and Carry */
				Carry = CombineTrees( T2, Carry );
				H2->TheTrees[ i ] = NULL;
				break;
			case 7: /* All three */
				H1->TheTrees[ i ] = Carry;
				Carry = CombineTrees( T1, T2 );
				H2->TheTrees[ i ] = NULL;
				break;
		}
	}
	return H1;
}

// by Borahan T�mer
BinTree printTree(BinTree p, BinTree *r, int i)
{
  BinTree t[20]={NULL}, q; int j;
  for ( j=0; j<i; j++ ) t[j]= r[j];
  i=0;
  if (p!=NULL) { 
	printf("& %2.1lf ",p->Item);
	q=p->NextSibling;
	j=0;
	do {
	  while (q!=NULL) { 
		printf("%2.1lf ",q->Item); 
		if (q->LeftChild != NULL) { r[i]=q->LeftChild; i++; }
		q=q->NextSibling;
	  }
	  q=t[j++];
	} while (q!=NULL);
  }
  else return NULL;
  //for (j=0; j<i; j++) t[j]=NULL;
  printf("\n");
  printTree(p->LeftChild, r, i);
}

		
//	/*
typedef struct process {
    int e;
    int tarr;
    char processName[10];
	bool ekle;
} process ;

main()
{
  BinQueue H1, H2;
  BinTree p, r[20]={NULL};
  ElementType Item,time;
  char ch;
  int i,k;
  double temp;
  double awt=0;
  H1 = Initialize( );
  // Written by me 
  int q ,priority;
    process process[128];
    FILE *f;
    f = fopen ("input.txt","rw+");
    int j;
	for ( j = 0; j != 6 && fscanf(f, "%s %d %d", &process[j].processName, &process[j].e, &process[j].tarr ) != EOF; j++);
    fclose (f);
	
	for(j=0;j<6;j++){
	process[j].ekle=false;
	printf("%s,%d\n",process[j].processName,process[j].e);
	}
	// Calculating eMax
	double eMax;
	for(j=0;j!=6;j++){
		eMax=0;
		if(process[j].e>eMax)
			eMax=process[j].e;
	}	
	//
	time=0;
	for(q=3;q<4;q++){
		while(time<400){
			printf("Time = %f\n",time);
			for(j=0;j<6;j++){
				if(process[j].tarr<=time && process[j].ekle==false){
				Insert(process[j].e,process[j].tarr,true,H1);
				process[j].ekle=true;
				}
			} 
			for (i=0; i<MaxTrees; i++) {
			for (i=0; i<MaxTrees; i++) {
			              p=H1->TheTrees[i];
						  printTree(p, r, 0); printf("/\n");
						}  
			if(FindMin(H1)>q){
				printf("01\n");
				eMax=25.0;
				temp=FindMin(H1)-q;
				double cube=exp(-(double)(2.0*(temp)/(3.0*eMax))*(2.0*(temp)/(3.0*eMax))*(2.0*(temp)/(3.0*eMax)));
				temp=(1.0/cube)*temp;
				if(temp<1){
					for(k=0;k<10;k++)
					{
						if(H1->TheTrees[k] && H1->TheTrees[k]->Item==FindMin(H1))
						{
						if(H1->TheTrees[k]->Time!=time){
						awt=awt+time-H1->TheTrees[k]->Time;
						printf("Awt = %f\n",(awt/,j);
						}
						}
					}
				for(k=0;k<10;k++)
					{
						if(H1->TheTrees[k] && H1->TheTrees[k]->Item==FindMin(H1))
						{
							if(H1->TheTrees[k]->Item>q)
							time=time+q;
							else if(H1->TheTrees[k]->Item==1)
							time=time+1;
						}
						
					}
					
					DeleteMin( H1 );
					
				}	
				else{
					for(k=0;k<10;k++)
					{
						if(H1->TheTrees[k] && H1->TheTrees[k]->Item==FindMin(H1))
						{
						if(H1->TheTrees[k]->Time!=time){
						awt=awt+time-H1->TheTrees[k]->Time;
						printf("Awt = %f\n",(awt/,j);
						}
						}
					}
					for(k=0;k<10;k++)
					{
						if(H1->TheTrees[k] && H1->TheTrees[k]->Item==FindMin(H1))
						{
							if(H1->TheTrees[k]->Item>q)
							time=time+q;
							else if(H1->TheTrees[k]->Item==1)
							time=time+1;
						}
						
					}
					DeleteMin( H1 );
					
					if(temp>=1){
					Insert(temp,time,true,H1);
					}
					
				}
					
				}
			else 
			{
				printf("1");
				for(k=0;k<3;k++)
					{
						if(H1->TheTrees[k] && H1->TheTrees[k]->Item==FindMin(H1))
						{
						if(H1->TheTrees[k]->Time!=time){
						awt=awt+time-H1->TheTrees[k]->Time;
						printf("Awt = %f\n",(awt/,j);
						}
						}
					}
				for(k=0;k<3;k++)
					{
						if(H1->TheTrees[k] && H1->TheTrees[k]->Item==FindMin(H1))
						{
							if(H1->TheTrees[k]->Item>q)
							time=time+q;
							else if(H1->TheTrees[k]->Item==1)
							time=time+1;
							else if(H1->TheTrees[k]->Item<q)
							time=(int)time+H1->TheTrees[k]->Item;
							else
							time=time+q;
						}
						
					}
				DeleteMin( H1 );
			}

			
				
		}
		printf("Awt = %f\n",(awt));
	}

	
   //////// 

}                                         
}